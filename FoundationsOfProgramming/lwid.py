#!/usr/bin/env python

"""
Input from argv
Input: S D1 D2 D3 ... DN
Output: find the longest word in DN that is a subsequence of S.
"""

import sys

# upgraded solution 
def lwidUpgraded(S, D):
    max_word="None"
    max_len=0

    s_dict = convertStrToDictWithIndex(S)
    print(s_dict)

    for word in sorted(D, key=lambda w: len(w), reverse=True):
        index=0
        word_len = len(word)
        
        for char in word:
            x = searchMinGreatherEqualThanIndex(s_dict.get(char), index)
            if x >= index:
                index = x + 1
            else:
                break
        else:
            print(word + " - " + str(word_len))
            return
    print(max_word)


# greedy approach
def lwidGreedy(S, D):
    max_word="None"
    max_len=0

    s_dict = convertStrToDictWithIndex(S)

    for word in D:
        index=0
        word_len = len(word)

        if word_len <= max_len or word_len > len(S):
            continue
        for char in word:
            x = searchMinGreatherEqualThanIndex(s_dict.get(char), index)
            if x >= index:
                index = x
            else:
                index = 0
                break
        if word_len > max_len and index > 0:
            max_len = word_len
            max_word = word

        print(max_word + " - " + str(max_len))
        
def convertStrToDictWithIndex(str):
    str_dict = {}
    for i in range(len(str)):
        if str[i] in str_dict.keys():
            str_dict[str[i]].append(i)
        else:
            str_dict[str[i]] = [i]
    return str_dict

# O(log2 n)
def searchMinGreatherEqualThanIndex(arr, index):
    if arr == None:
        return -1

    left, right = 0, len(arr)
    last_match = -1
    while left < right:
        mid = (left + right) // 2
        if arr[mid] == index:
            return index
        elif arr[mid] > index:
            if last_match == -1 or last_match > arr[mid]:
                last_match = arr[mid]
            right = mid
        else:
            left = mid

    return last_match
 
# first approach
def lwid(S, D):
    max_word="None"
    max_len=0

    for word in D:
        i_word=0
        word_len = len(word)
        if word_len <= max_len or word_len > len(S):
            continue
        for letter in S:
            if word[i_word] == letter:
                i_word+=1
            if i_word >= word_len:
                if word_len > max_len:
                    max_len = word_len
                    max_word = word    
                break

        print(max_word + " - " + str(max_len))
 

if __name__ == '__main__':
    if len(sys.argv) > 1:
        S=sys.argv[1:][0]
        D=sys.argv[1:][1:]
        lwidUpgraded(S, D)
    else:
        print("invalid input")