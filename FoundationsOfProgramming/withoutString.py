#!/usr/bin/env python

def withoutString(str, toRemove):
    wstr=""
    
    if len(toRemove) == 0 or len(str) == 0:
        return str

    i = 0
    while i < len(str):
        if str[i] != toRemove[0]:
            wstr+= str[i]
        elif i < len(str)-len(toRemove)+1:
            prev=""
            for j in range(len(toRemove)):
                if str[i + j] == toRemove[j]:
                    prev+=str[i+j]
                else:
                    wstr+=prev
                    i+=j
                    prev=""
                    break
            else:
                i+=j
        else:
            wstr+=str[i]

        i+=1

    return wstr

print(withoutString("Hello there", "llo")) # "He there"
print(withoutString("Hello there", "e")) # "Hllo thr"
print(withoutString("Hello there", "x")) # "Hello there"
print(withoutString("abcabccba", "ab")) # "cccba"
print(withoutString("123456789999", "9")) # "12345678"
print(withoutString("aabaaabbaaaa", "aaa")) # "aabba"
print(withoutString("aabaaabbaaaa", "")) # "aabba"
print(withoutString("", "")) # "aabba"