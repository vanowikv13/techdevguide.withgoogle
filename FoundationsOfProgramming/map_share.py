#!/usr/bin/env python

'''
Modify and return the given map as follows: if the key "a" has a value,
set the key "b" to have that same value.
In all cases remove the key "c", leaving the rest of the map unchanged.
'''


def mapShare(map):
    if 'a' in map.keys():
        map['b'] = map.get('a')
    # pop a key if exist if not return None
    map.pop('c', None)
    return map


print(mapShare({"a": "aaa", "b": "bbb", "c": "ccc"}))
print(mapShare({"b": "xyz", "c": "ccc"}))
print(mapShare({"a": "aaa", "c": "meh", "d": "hi"}))
