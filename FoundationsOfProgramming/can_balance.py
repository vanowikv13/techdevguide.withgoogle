#!/usr/bin/env python

'''
Given a non-empty array, return true if there is a place to split
the array so that the sum of the numbers on one side is equal
to the sum of the numbers on the other side.
'''

# this version works only for positive numbers
def canBalance(arr):
    if len(arr) < 2:
        return False

    i = 1
    j = len(arr)-2

    leftSum=arr[0]
    rightSum=arr[j+1]
    while i <= j:
        if leftSum > rightSum:
            rightSum+=arr[j]
            j-=1
        else:
            leftSum+=arr[i]
            i+=1
    if leftSum == rightSum:
        return True
    return False

# works for negative and positive numbers, but it's slower
def canBalanceWithNegative(arr):
    if len(arr) < 2:
        return False

    rightSum = 0
    leftSum = arr[0]
    for i in range(1, len(arr)):
        rightSum += arr[i]

    for i in range(1, len(arr)-1):
        if rightSum == leftSum:
            return True
        else:
            leftSum += arr[i]
            rightSum -= arr[i]

    if rightSum == leftSum:
            return True
    return False

assert True == canBalance([1, 1, 1, 2, 1])
assert False == canBalance([2, 1, 1, 2, 1])
assert True == canBalance([10, 10])
assert True == canBalanceWithNegative([10, 0, 1, -1, 10])
assert True == canBalance([1, 1, 1, 1, 4])
assert False == canBalance([2, 3, 4, 1, 2])
assert True == canBalance([1, 2, 3, 1, 0, 2, 3])
assert False == canBalance([1, 2, 3, 1, 0, 1, 3])
assert False == canBalance([1])
assert True == canBalance([1, 1, 1, 2, 1])