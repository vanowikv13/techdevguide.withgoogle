#!/usr/bin/env python

'''
Return an array that contains the sorted values from the input array with duplicates removed.
'''

# the whole thing could be realize with set structure
def sort(arr):
    for i in range(len(arr)):
        for j in range(i+1, len(arr)):
            if arr[j] < arr[i]:
                tmp = arr[i]
                arr[i] = arr[j]
                arr[j] = arr[i]
    newArr = []
    for i in range(len(arr)):
        if arr[i] not in newArr:
            newArr.append(arr[i])
    return newArr


print(sort([]))
print(sort([1]))
print(sort([1, 1]))
print(sort([2, 1]))
print(sort([2,1,2]))