#!/usr/bin/env python
from random import randint

"""
Generating MinSweeper field.
"""

# CONSTANT
BOMB_AROUND_VALUE = 1
BOMB_DESCRIPTOR = 9


def generateMinSweeper(height, widths, bombs):
    if bombs >= height * widths:
        raise Exception("Wrong bombs size")

    m = [[0 for i in range(widths)] for i in range(height)]

    m = placeBombs(m, height, widths, bombs)
    return m


def placeBombs(matrix, height, widths, bombs):
    for i in range(bombs):
        while True:
            x = randint(0, height-1)
            y = randint(0, widths-1)
            if matrix[x][y] != 9:
                matrix[x][y] = 9
                break
        if x > 0:
            # TOP
            checkIfNotBombAndAddValue(matrix, x-1, y)
            if y > 0:
                # TOP-LEFT
                checkIfNotBombAndAddValue(matrix, x-1, y-1)
            if y < widths-1:
                # TOP-RIGHT
                checkIfNotBombAndAddValue(matrix, x-1, y+1)
        if x < height-1:
            # BOTTOM
            checkIfNotBombAndAddValue(matrix, x+1, y)
            if y > 0:
                # BOTTOM-LEFT
                checkIfNotBombAndAddValue(matrix, x+1, y-1)
            if y < widths-1:
                # BOTTOM-RIGHT
                checkIfNotBombAndAddValue(matrix, x+1, y+1)
        if y > 0:
            # LEFT
            checkIfNotBombAndAddValue(matrix, x, y-1)
        if y < widths-1:
            # RIGHT
            checkIfNotBombAndAddValue(matrix, x, y+1)
    return matrix


def checkIfNotBombAndAddValue(arr, x, y):
    if arr[x][y] != BOMB_DESCRIPTOR:
        arr[x][y] += BOMB_AROUND_VALUE


m = generateMinSweeper(10, 10, 11)
for arr in m:
    print(arr)
