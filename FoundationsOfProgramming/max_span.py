'''
Consider the leftmost and rightmost appearances of some value in an array.
We'll say that the "span" is the number of elements between the two inclusive.
A single value has a span of 1. Returns the largest span found in the given array.
'''

def maxSpan(arr):
    arr_len = len(arr)
    max_span = 0

    for i in range(arr_len-1):
        j = arr_len-1
        while j > i and (j - i + 1) > max_span:
            if arr[i] == arr[j]:
                span = j - i + 1
                if max_span <  span:
                    max_span = span
            j-=1
    return max_span

assert maxSpan([1, 2, 1, 1, 3]) == 4
assert maxSpan([1, 4, 2, 1, 4, 1, 4]) == 6
assert maxSpan([1, 4, 2, 1, 4, 4, 4]) == 6