#!/usr/bin/env python

# Unit tests for the algorithms could be find in test_map.py

"""
Map1:
Given an array of strings, return a Map<String, Integer> containing a key
for every different string in the array, and the value is that string's length.
"""


def wordLen(arr):
    map_ = {}
    for word in arr:
        if word not in map_.keys():
            map_[word] = len(word)
    return map_


"""
Map2:
Write a function that replaces the words in `raw` with the words in `code_words`
such that the first occurrence of each word in `raw` is assigned the first
unassigned word in `code_words`.
"""

def encoder(raw, code_words):
    map_ = {}
    new_arr = []

    i = 0
    while i < len(raw) and i < len(code_words):
        if raw[i] not in map_.keys():
            new_arr.append(code_words[i])
            map_[raw[i]] = code_words[i]
        else:
            new_arr.append(map_[raw[i]])
        i += 1
    return new_arr
