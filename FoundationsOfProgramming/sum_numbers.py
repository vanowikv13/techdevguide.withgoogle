#!/usr/bin/env python

def sumNumbers(string):
    strsum = 0
    x = ""
    for char in string:
        if ord(char) >= 48 and ord(char) <= 57:
            x += char
        else:
            if len(x) > 0:
                strsum += int(x)
                x = "" 
    else: 
        if len(x) > 0:
            strsum += int(x)

    return strsum


assert 123 == sumNumbers("abc123xyz") # 123
assert 44 == sumNumbers("aa11b33") # 44
assert 19 == sumNumbers(" 9 10") # 19
assert 0 == sumNumbers("") # 0