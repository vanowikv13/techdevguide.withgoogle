from unittest import TestCase
from map import encoder, wordLen


class TestWorldLen(TestCase):
    def test(self):
        self.assertEqual(wordLen(["a", "bb", "a", "bb"]), {"bb": 2, "a": 1})
        self.assertEqual(wordLen(["this", "and", "that", "and"]), {"that": 4, "and": 3, "this": 4})
        self.assertEqual(wordLen(["code", "code", "code", "bug"]), {"code": 4, "bug": 3})


class TestEncoder(TestCase):
    def test(self):
        self.assertEqual(encoder(["a"], ["1", "2", "3", "4"]), ["1"])
        self.assertEqual(encoder(["a", "b"], ["1", "2", "3", "4"]), ["1", "2"])
        self.assertEqual(encoder(["a", "b", "a"], ["1", "2", "3", "4"]), ["1", "2", "1"])
        self.assertEqual(encoder(["a", "b", "c"], ["1", "2", "3", "4"]), ["1", "2", "3"])
        self.assertEqual(encoder(["a", "b", "a"], ["1", "2", "3", "4"]), ["1", "2", "1"])
        self.assertEqual(encoder(["a", "b", "b"], ["1", "2", "3", "4"]), ["1", "2", "2"])