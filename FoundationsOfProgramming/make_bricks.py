#!/usr/bin/env python

"""
We want to make a row of bricks that is goal inches long.
We have a number of small bricks (1 inch each) and big bricks (5 inches each).
Return True if it is possible to make the goal by choosing from the given bricks.
"""

def makeBricks(small, big, goal):
    sum = 0
    if goal//5 >= big:
        sum += big * 5
    else:
        sum += (goal//5) * 5
    
    if goal - sum - small > 0:
        return False
    return True

assert makeBricks(3, 1, 8) == True
assert makeBricks(3, 1, 9) == False
assert makeBricks(3, 2, 10) == True
assert makeBricks(1000000, 1000, 1000100) == True
assert makeBricks(20, 0, 19) == True
assert makeBricks(20, 4, 51) == False
assert makeBricks(0, 3, 10) == True