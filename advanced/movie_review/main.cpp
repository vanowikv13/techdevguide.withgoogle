#include "movieReview.h"


void printWord(std::string word, int amount, int sum)
{
    std::cout << word << " appears " << amount << " times\nAverage score for reviews containing the word " << word << " ";
    if (amount == 0)
        std::cout << 0 << "\n";
    else
        std::cout << static_cast<float>(sum) / amount << "\n";
}

void printWordsSummary(std::string fileName, float score)
{
    std::cout << "The average score of words in " + fileName + " is " << score << std::endl;
    std::cout << "The overall sentiment of " + fileName + " is ";

    if (score < 2.0)
        std::cout << "negative\n";
    else if (score >= 2.0 && score < 3.5)
        std::cout << "neutral\n";
    else if (score >= 3.5)
        std::cout << "positive\n";
    else
        std::cout << "Wrong score";
}

void printMostPositiveAndNegative(std::pair<std::string, float> negative, std::pair<std::string, float> positive) {
    std::cout << "The most positive word, with score: " << positive.second << " is " << positive.first << std::endl;
    std::cout << "The most negative word, with score: " << negative.second << " is " << negative.first << std::endl;
}

int main()
{
    MovieReview rev = MovieReview("movieReviews.txt", "wordList.txt");
    std::string str;

    while (true)
    {
        int option = 0;
        std::cout << "Your options:\n";
        std::cout << "1) Check a word average score\n";
        std::cout << "2) Check a file with words and give average score\n";
        std::cout << "3) Find most positive and negative word in a file with words\n";
        std::cout << "4) Exit\n";
        std::cout << "Chose option: ";
        std::cin >> option;

        switch (option)
        {
        case 1:
            std::cout << "Enter a word: ";
            std::cin >> str; 
            {
                auto x = rev.reviewAWord(str);
                printWord(str, x.first, x.second);
            }
            break;
        case 2:
            std::cout << "Enter a file name: ";
            std::cin >> str;
            {
            float avg = rev.reviewAListOfWordsFromFile(str);
            printWordsSummary(str, avg);
            }
            break;
        case 3:
            std::cout << "Enter a file name: ";
            std::cin >> str;
            {
            auto posNeg = rev.reviewMostPositiveAndNegativeWordInFile(str);
            printMostPositiveAndNegative(posNeg.first, posNeg.second);
            }
            break;

            break;
        case 4:
            return 0;
        }
        
        std::cout << std::endl;
    }

    return 0;
}