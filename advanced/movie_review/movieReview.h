#ifndef MOVIE_REVIEW_H
#define MOVIE_REVIEW_H
#include <fstream>
#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <limits>
// pair
#include <utility>

const std::string WORD_LIST_FILE_NAME = "wordList.txt";

class MovieReview
{
    // word: (amount, sum)
    std::map<std::string, std::pair<int, int>> words_review;
    std::fstream file_handler;

public:
    MovieReview(std::string moveReview, std::string wordList = WORD_LIST_FILE_NAME)
    {
        setUpWordList(moveReview, wordList);
    }

    // amount, sum
    std::pair<int, int> reviewAWord(std::string word)
    {
        auto x = words_review.find(word);
        if (x == words_review.end())
            return std::pair<int, int>(0, 0);
        return words_review[word];
    }

    std::pair<std::pair<std::string, float>, std::pair<std::string, float>> reviewMostPositiveAndNegativeWordInFile(std::string fileName) {
        std::pair<std::string, float> max = std::pair<std::string, float>("", 0.0),
                                      min = std::pair<std::string, float>("", std::numeric_limits<float>::max());
        std::string word;

        file_handler.open(fileName, std::ios::in);
        while (file_handler >> word)
        {
            auto x = words_review.find(word);
            if (x != words_review.end())
            {
                float avg = static_cast<float>((x->second.second)) / (x->second.first);
                if (avg > max.second)
                    max = std::pair<std::string, float>(word, avg);

                if (avg < min.second)
                    min = std::pair<std::string, float>(word, avg);
            }
        }
        file_handler.close();
        return std::pair<std::pair<std::string, float>, std::pair<std::string, float>>(min, max);
    }

    float reviewAListOfWordsFromFile(std::string fileName)
    {
        float sum = 0.0;
        int amount = 0;
        std::string word;

        file_handler.open(fileName, std::ios::in);
        while (file_handler >> word)
        {
            auto x = words_review.find(word);
            if (x != words_review.end())
            {
                sum += static_cast<float>((x->second.second)) / (x->second.first);
                amount++;
            }
        }
        file_handler.close();
        return (sum / amount);
    }

private:
    void setUpWordList(std::string moveReview, std::string wordList)
    {
        file_handler.open(wordList, std::ios::in);
        if (file_handler.is_open())
        {
            getWordListFromFileStream();
            file_handler.close();
        }
        else
        {
            file_handler.open(moveReview, std::ios::in);
            if (file_handler.is_open())
            {
                setWordListFromMovieReview();
                file_handler.close();

                file_handler.open(wordList, std::ios::out | std::ios::trunc);
                for (auto const &it : words_review)
                {
                    file_handler << it.first << " " << (it.second.first) << " " << it.second.second << "\n";
                }

                file_handler.close();
            }
            else
                exit(-1);
        }
    }

    void getWordListFromFileStream()
    {
        std::string word;
        int amount = 0;
        int sum = 0;

        while (file_handler >> word >> amount >> sum)
            words_review[word] = std::pair<int, int>(amount, sum);
    }

    void setWordListFromMovieReview()
    {
        std::string line;
        std::string num;

        while (std::getline(file_handler, line))
        {
            num = "";
            int i = 0;
            for (; i < line.size(); i++)
            {
                if (line[i] == ' ')
                    break;
                num += line[i];
            }
            addWordsToTheList(std::stoi(num), line.substr(i));
        }
    }

    void addWordsToTheList(int grade, std::string review)
    {
        std::string word = "";
        for (int i = 0; i < review.size(); i++)
        {
            char chr = std::tolower(review[i]);
            if (review[i] != ' ' && static_cast<int>(chr) >= 97 && static_cast<int>(chr) <= 122)
            {
                word += chr;
            }
            else if (review[i] == ' ')
            {
                if (words_review.find(word) == words_review.end())
                {
                    if (word.size() > 0)
                        words_review[word] = std::pair<int, int>(1, grade);
                }
                else
                {
                    words_review[word].first++;
                    words_review[word].second += grade;
                }
                word = "";
            }
        }
    }
};

#endif //MOVIE_REVIEW_H