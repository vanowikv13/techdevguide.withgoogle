#!/usr/bin/env python

# Challenge description:
# https://techdevguide.withgoogle.com/paths/advanced/compress-decompression/#code-challenge

def decompress(c):
    return decomp(c, "1", 0)[0]

def decomp(compressed, repeat_times="1", bracket_starts=0):
    decompressed = ""

    # repeat next substring
    times="0"
    i = bracket_starts
    while i < len(compressed):
        if compressed[i] == '[':
            part_decomp = decomp(compressed, int(times), i + 1)
            times="0"
            i = part_decomp[1]
            decompressed += part_decomp[0]
        elif compressed[i] == ']':
            decompressed = repeat_times*decompressed
            return (decompressed, i)
        else:
            # checking if character is a number between: <0, 1 ..., 9>
            if ord(compressed[i]) >= 48 and ord(compressed[i]) <= 57:
                times += compressed[i]
            else:
                decompressed += compressed[i]
        i += 1
    return (decompressed, i)

# Examples:
assert decompress("3[abc]4[ab]c") == "abcabcabcababababc"
assert decompress("2[3[a]b]") == "aaabaaab"
assert decompress("10[a]") == "aaaaaaaaaa"