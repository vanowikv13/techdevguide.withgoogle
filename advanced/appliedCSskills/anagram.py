#!/usr/bin/env python
import os
import sys
from random import randrange
from enum import Enum

class DIFFICULTY(Enum):
    EASY=(3, 4)
    MEDIUM=(5, 7)
    HARD=(8, sys.maxsize)

    def increment(self, x):
        if x == DIFFICULTY.EASY:
            return DIFFICULTY.MEDIUM
        elif x == DIFFICULTY.MEDIUM:
            return DIFFICULTY.HARD
        else:
            return DIFFICULTY.EASY

class AnagramGame:
    def __init__(self):
        self.words = self.getWordsFromFile()
        self.ANAGRAM_GAME_DIFFICULTY = DIFFICULTY.EASY

    def start(self):
        while True:
            word = self.getRandomWord()
            anagrams = self.findAnagramWithOneLetterDiffInFile(word)

            print("Hello Traveler, It's time to find some anagram.")
            print("Here is your word: " + word)
            print("Find as many anagrams + one letter as possible")
            print("scheme: anagram[enter]")
            print("To finish writing anagrams use: ;;[enter]")

            user_anagrams = []
            while True:
                x = input()
                if x == ';;':
                    break
                user_anagrams.append(x)

            amount_of_correct = 0
            for anagram in anagrams:
                if anagram in user_anagrams:
                    print(anagram + " | correct")
                    amount_of_correct += 1
                else:
                    print(anagram + " | not found")

            for anagram in user_anagrams:
                if anagram not in anagrams:
                    print(anagram + " | wrong")

            print("Your score: " + str(amount_of_correct) +
                "/" + str(len(anagrams)))
            print("press [enter] to start another round")
            input()
            os.system("clear")
            self.ANAGRAM_GAME_DIFFICULTY = DIFFICULTY.increment(DIFFICULTY, self.ANAGRAM_GAME_DIFFICULTY)


    def getWordsFromFile(self, fileName="words.txt"):
        file = open(fileName, "r")
        content = file.read().splitlines()
        file.close()
        return content

    def findAnagramWithOneLetterDiffInFile(self, base):
        anagrams = []
        for word in self.words:
            if self.isAnagramWithOneLetterDiff(base, word):
                anagrams.append(word)

        return anagrams

    @staticmethod
    def isAnagramWithOneLetterDiff(base, anagram):
        # checking if user add one letter at the beggining or end and do not rotate rest
        if anagram.find(base) != -1 or len(base) >= len(anagram) or len(base)+1 < len(anagram):
            return False

        base = base.lower()
        anagram = anagram.lower()

        base_dict = AnagramGame.dictFromString(base)
        anagram_dict = AnagramGame.dictFromString(anagram)

        diff = 0
        for key, value in base_dict.items():
            diff += abs(value - anagram_dict.get(key, 0))
            if diff > 1:
                return False

        for key, value in anagram_dict.items():
            if key not in base:
                diff += value
            if diff > 1:
                return False

        if diff > 1:
            return False
        return True

    @staticmethod
    def dictFromString(string):
        dict = {}

        for char in string:
            if char not in dict.keys():
                if char != '\n':
                    dict[char] = 1
            else:
                dict[char] += 1
        return dict

    def getRandomWord(self):
        while True:
            rand_index = randrange(0, len(self.words)-1)
            length = len(self.words[rand_index])
            if length >= self.ANAGRAM_GAME_DIFFICULTY.value[0] and length <= self.ANAGRAM_GAME_DIFFICULTY.value[1]:
                return self.words[rand_index]

    # just another version of anagram algorithm with sort
    @staticmethod
    def isAnagramWithOneLetterDiffSort(base, anagram):
        # checking if user add one letter at the beggining or end and do not rotate rest
        if anagram.find(base) != -1 or len(base) >= len(anagram) or len(base)+1 < len(anagram):
            return False

        base = ''.join(sorted(base))
        anagram = ''.join(sorted(anagram))

        diff = 0
        i, j = 0, 0
        while i < len(base) and j < len(anagram):
            if diff > 1:
                return False

            if base[i] != anagram[j]:
                diff += 1
                if j + 1 < len(anagram) and anagram[j + 1] == base[i]:
                    j += 1
                elif i + 1 < len(base) and anagram[j] == base[i + 1]:
                    i += 1
                else:
                    diff += 1
                    j += 1
                    i += 1
            else:
                i += 1
                j += 1
        if diff > 1:
            return False
        return True


if __name__ == "__main__":
    anagramGame = AnagramGame()
    anagramGame.start()
